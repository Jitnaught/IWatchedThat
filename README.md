Remembers videos you've watched in a directory and hides them within the program. Written in C# using Mono (.NET Framework version 4.0), GTK# 2.12, and the [INI parser by rickyah](https://github.com/rickyah/ini-parser).

Uses Mono.Data.Sqlite to create a database (named watched.sqlite) within each directory you select. 
Double-click a video to watch it, and when you're done go back to the program and it will ask if you want to set the video as watched. If you set it to watched the video is added to the database.
You can also right-click videos to set them as watched/not watched.
If you delete or rename a watched video then it will not be remembered (the entry is deleted from the database).

Requirements:
Linux: mono, gtk-sharp2
Windows: [GTK#](http://www.mono-project.com/download/#download-win)
Mac: (untested) [Mono](http://www.mono-project.com/download/#download-mac)

[Download.](https://gitlab.com/Jitnaught/IWatchedThat/tags)

P.S. If you are building from source using MonoDevelop on Linux make sure to remove the Mono.Posix reference, otherwise the program won't run under Windows.

![Main window](https://i.imgur.com/1PNxRGv.jpg)

![Main window show watched](https://i.imgur.com/YZbuDTW.jpg)

![Open directory](https://i.imgur.com/SK1iYB9.jpg)
