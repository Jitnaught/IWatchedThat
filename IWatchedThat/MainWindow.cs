﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Gtk;
using IniParser;
using Mono.Data.Sqlite;

public partial class MainWindow : Window
{
    const string DB_NAME = "watched.sqlite";
    const string VIDEO_FORMATS = "*.m2v|*.m4v|*.mpeg1|*.mpeg2|*.mts|*.divx|*.dv|*.flv|*.m1v|*.m2ts|*.mkv|*.mov|*.mpeg4|*.ts|*.vob|*.3g2|*.avi|*.mpeg|*.mpg|*.3gp|*.wmv|*.asf|*.mp4";

    TreeStore tvVideos_TreeStore;

    TreeViewColumn fileNameColumn, watchedColumn;
    CellRendererText crtFileName, crtWatched;

    Menu menu;

    string iniPath;

    string lastLocation = string.Empty;

    string openedDir = string.Empty;
    string videoPlayed = string.Empty;

    SqliteConnection dbConnection = null;
    List<string> watchedFiles = new List<string>();

    string selectedItem = string.Empty;

    public MainWindow() : base(WindowType.Toplevel)
    {
        Build();

        //treeview
        tvVideos_TreeStore = new TreeStore(typeof(string), typeof(bool));

        fileNameColumn = new TreeViewColumn() { Title = "File", Resizable = true, Sizing = TreeViewColumnSizing.Fixed, FixedWidth = 350, Visible = true };
        watchedColumn = new TreeViewColumn() { Title = "Watched", Resizable = true, Visible = false };

        tvVideos.AppendColumn(fileNameColumn);
        tvVideos.AppendColumn(watchedColumn);

        tvVideos_TreeStore.SetSortColumnId(0, SortType.Ascending);

        tvVideos.Model = tvVideos_TreeStore;

        crtFileName = new CellRendererText();
        crtWatched = new CellRendererText();

        fileNameColumn.PackStart(crtFileName, true);
        watchedColumn.PackStart(crtWatched, true);

        fileNameColumn.AddAttribute(crtFileName, "text", 0);
        watchedColumn.AddAttribute(crtWatched, "text", 1);

        //context menu for treeview
        menu = new Menu();

        MenuItem setWatchedItem = new MenuItem("Set as watched"), setNotWatchedItem = new MenuItem("Set as not watched");

        setWatchedItem.Activated += (sender, e) =>
        {
            setAsWatched(true);
        };

        setNotWatchedItem.Activated += (sender, e) =>
        {
            setAsWatched(false);
        };

        menu.Append(setWatchedItem);
        menu.Append(setNotWatchedItem);

        menu.ShowAll();

        //ini
        iniPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "settings.ini");

        if (File.Exists(iniPath))
        {
            var ini = new FileIniDataParser().ReadFile(iniPath);

            lastLocation = ini.Sections.GetSectionData("SETTINGS").Keys.GetKeyData("LAST_LOCATION").Value;
        }
        else
        {
            lastLocation = "";

            var iniData = new IniParser.Model.IniData();
            var sectionData = new IniParser.Model.SectionData("SETTINGS");
            sectionData.Keys.AddKey("LAST_LOCATION", lastLocation);
            iniData.Sections.Add(sectionData);

            new FileIniDataParser().WriteFile(iniPath, iniData);
        }

        if (lastLocation != "" && Directory.Exists(lastLocation))
        {
            openedDir = lastLocation;
            loadDir(lastLocation);
        }
        else
        {
            browseDir();
        }
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        if (dbConnection != null) dbConnection.Close();

        Application.Quit();
        a.RetVal = true;
    }

    protected void MainWindow_FocusIn(object sender, EventArgs e)
    {
        if (videoPlayed != string.Empty)
        {
            this.FocusInEvent -= MainWindow_FocusIn;

            if (!watchedFiles.Contains(videoPlayed))
            {
                MessageDialog dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo, "Set as watched?");
                dialog.Title = "Watched?";

                dialog.Response += (object o, ResponseArgs args) =>
                {
                    if (args.ResponseId == ResponseType.Yes)
                    {
                        watchedFiles.Add(videoPlayed);
                        dbInsertWatchedFile(videoPlayed);

                        if (!cbShowWatched.Active)
                        {
                            TreeIter iter;
                            tvVideos.Selection.GetSelected(out iter);

                            tvVideos_TreeStore.Remove(ref iter);
                        }
                    }
                };

                dialog.Run();
                dialog.Destroy();
            }
        }

        videoPlayed = string.Empty;
    }

    protected void btnOpenDir_Click(object sender, EventArgs e)
    {
        browseDir();
    }

    protected void cbShowWatched_Toggled(object sender, EventArgs e)
    {
        if (cbShowWatched.Active)
        {
            watchedColumn.Visible = true;

            foreach (string file in watchedFiles)
            {
                tvVideos_TreeStore.AppendValues(new object[] { file, true });
            }
        }
        else
        {
            watchedColumn.Visible = false;

            TreeIter iter;
            tvVideos_TreeStore.GetIterFirst(out iter);

            do
            {
                string file = (string)tvVideos_TreeStore.GetValue(iter, 0);

                if (watchedFiles.Contains(file))
                {
                    tvVideos_TreeStore.Remove(ref iter);
                }
            }
            while (tvVideos_TreeStore.IterNext(ref iter));
        }
    }

    [GLib.ConnectBefore]
    protected void tvVideos_ButtonPressEvent(object sender, ButtonPressEventArgs e)
    {
        if (e.Event.Button == 1 && e.Event.Type == Gdk.EventType.TwoButtonPress) //left-click double-press
        {
            if (tvVideos.Selection.CountSelectedRows() > 0)
            {
                TreeIter iter;

                if (tvVideos.Selection.GetSelected(out iter))
                {
                    string file = (string)tvVideos_TreeStore.GetValue(iter, 0);

                    string path = System.IO.Path.Combine(openedDir, file);

                    if (File.Exists(path))
                    {
                        int platform = (int)Environment.OSVersion.Platform;

                        switch (platform)
                        {
                            //untested on windows and mac
                            case 4:
                            case 128: //linux
                                Process.Start("xdg-open", "\"" + path + "\"");
                                break;
                            case 6: //mac
                                Process.Start("open", "\"" + path + "\"");
                                break;
                            default: //windows
                                Process.Start("\"" + path + "\"");
                                break;
                        }

                        videoPlayed = file;

                        this.FocusInEvent += MainWindow_FocusIn;
                    }
                }
            }
        }
        else if (e.Event.Button == 3 && e.Event.Type == Gdk.EventType.ButtonPress) //right-click press
        {
            if (tvVideos.Selection.CountSelectedRows() > 0)
            {
                menu.Popup();
            }
        }
    }

    private static List<object[]> getFiles(string path, string searchPattern = "*.*", SearchOption searchOption = SearchOption.TopDirectoryOnly)
    {
        string[] searchPatterns = searchPattern.Split('|');
        var allFiles = new List<object[]>();

        foreach (string sp in searchPatterns)
        {
            var files = Directory.GetFiles(path, sp, searchOption);

            foreach (var file in files)
            {
                allFiles.Add(new object[] { file, false });
            }
        }

        return allFiles;
    }

    private void dbInsertWatchedFile(string fileName)
    {
        var command = new SqliteCommand("INSERT INTO watched (name) VALUES ( @filename )", dbConnection);
        command.Parameters.Add("@filename", DbType.String);
        command.Parameters["@filename"].Value = fileName;
        command.ExecuteNonQuery();
    }

    private void dbRemoveWatchedFile(string fileName)
    {
        var command = new SqliteCommand("DELETE FROM watched WHERE name = @filename", dbConnection);
        command.Parameters.Add("@filename", DbType.String);
        command.Parameters["@filename"].Value = fileName;
        command.ExecuteNonQuery();
    }

    private void dbRemoveWatchedFile(Int64 id)
    {
        var command = new SqliteCommand("DELETE FROM watched WHERE id = @id", dbConnection);
        command.Parameters.Add("@id", DbType.Int64);
        command.Parameters["@id"].Value = id;
        command.ExecuteNonQuery();
    }

    private void setAsWatched(bool watched)
    {
        if (tvVideos.Selection.CountSelectedRows() > 0)
        {
            TreeIter iter;

            if (tvVideos.Selection.GetSelected(out iter))
            {
                string file = (string)tvVideos_TreeStore.GetValue(iter, 0);

                int index = watchedFiles.IndexOf(file);

                if (index > -1)
                {
                    if (!watched)
                    {
                        tvVideos_TreeStore.SetValue(iter, 1, false);
                        watchedFiles.RemoveAt(index);
                        dbRemoveWatchedFile(file);
                    }
                }
                else if (watched)
                {
                    if (cbShowWatched.Active)
                    {
                        tvVideos_TreeStore.SetValue(iter, 1, true);
                    }
                    else
                    {
                        tvVideos_TreeStore.Remove(ref iter);
                    }

                    watchedFiles.Add(file);
                    dbInsertWatchedFile(file);
                }
            }
        }
    }

    private void loadDir(string uri)
    {
        if (dbConnection != null)
        {
            dbConnection.Close();
            GC.Collect();
        }

        tvVideos_TreeStore.Clear();
        watchedFiles.Clear();

        openedDir = uri;

        string dbPath = System.IO.Path.Combine(uri, DB_NAME);

        bool fileNonexistant = !File.Exists(dbPath);

        if (fileNonexistant)
        {
            SqliteConnection.CreateFile(System.IO.Path.Combine(uri, DB_NAME));
        }

        dbConnection = new SqliteConnection("Data Source=" + dbPath + ";Version=3;Compress=True;");
        dbConnection.Open();

        if (fileNonexistant)
        {
            new SqliteCommand("CREATE TABLE watched (id INTEGER PRIMARY KEY, name VARCHAR(10000))", dbConnection).ExecuteNonQuery();
        }

        var files = new List<object[]>(getFiles(uri, VIDEO_FORMATS));

        if (files.Count > 0)
        {
            for (int i = 0; i < files.Count; i++)
            {
                files[i][0] = ((string)files[i][0]).Replace(openedDir + System.IO.Path.DirectorySeparatorChar, "");
            }

            var reader = new SqliteCommand("SELECT * FROM watched", dbConnection).ExecuteReader();

            while (reader.Read())
            {
                string name;

                if (reader.IsDBNull(1) ||
                    String.IsNullOrWhiteSpace(name = (string)reader["name"]) ||
                    !File.Exists(System.IO.Path.Combine(openedDir, name)))
                {
                    dbRemoveWatchedFile((Int64)reader["id"]);
                }
                else
                {
                    watchedFiles.Add(name);

                    int index = files.FindIndex((obj) =>
                    {
                        return ((string)obj[0]) == name;
                    });

                    if (index > -1)
                    {
                        files.RemoveAt(index);
                    }
                }
            }

            foreach (object[] row in files)
            {
                tvVideos_TreeStore.AppendValues(row);
            }

            selectedItem = string.Empty;
        }
    }

    private void browseDir()
    {
        var chooser = new FileChooserDialog("Choose a directory", this, FileChooserAction.SelectFolder,
                                            "Open", ResponseType.Ok,
                                            "Cancel", ResponseType.Cancel);

        chooser.Response += (object o, ResponseArgs args) =>
        {
            if (args.ResponseId == ResponseType.Ok && chooser.Filename != null && chooser.Filename != String.Empty && Directory.Exists(chooser.Filename))
            {
                openedDir = chooser.Filename;

                var parser = new FileIniDataParser();
                var ini = parser.ReadFile(iniPath);

                ini.Sections.GetSectionData("SETTINGS").Keys.GetKeyData("LAST_LOCATION").Value = openedDir;
                parser.WriteFile(iniPath, ini);

                loadDir(openedDir);
            }

            this.Activate();
        };

        chooser.Run();
        chooser.Destroy();
    }
}
